﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RangerTrainingAuth.Data;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.GruppenUebersicht;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace RangerTrainingAuth.Services
{
    public class GruppenUebersichtService
    {
        private readonly  ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public GruppenUebersichtService(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task< GruppenuebersichtModel> GetAsync()
        {
            GruppenuebersichtModel uebersicht = new GruppenuebersichtModel();



            var Trainingsgruppen = await _context.Spieler.Include(s=>s.Spielpositionen)
                                                         .Include(s => s.User)
                                                         .Where(s => s.Trainingsgruppe > 0 && s.User != null && s.Deaktiviert== false)
                                                         .OrderBy(s => s.Trainingsgruppe)
                                                         .ToArrayAsync();


            List<SpielerInGruppe> spielerInGruppe = new List<SpielerInGruppe>();

            foreach(Spieler sp in Trainingsgruppen)
            {
                SpielerInGruppe spig = new SpielerInGruppe();

                spig.Gruppe = sp.Trainingsgruppe;
                spig.Name = sp.Name;
                if (sp.User != null)
                {

                    spig.Gruppenleiter = (await userManager.IsInRoleAsync(sp.User, Rolename.Gruppenleiter)) ? "Gruppenleiter" : "";
                }
                else
                {
                    spig.Gruppenleiter = "user deaktiv";
                }

                spielerInGruppe.Add(spig);
            }

            uebersicht.Trainingsgruppen = spielerInGruppe.OrderBy(s => s.Gruppe)
                                                             .ThenByDescending(s => s.Gruppenleiter)
                                                             .ThenBy(s => s.Name)
                                                            .ToList();

            return uebersicht;

          //  return await Task.FromResult(uebersicht);
        }
    }
}
