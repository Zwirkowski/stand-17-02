﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RangerTrainingAuth.Data;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.Kalender;
using RangerTrainingAuth.Models.KalenderEintrag;

namespace RangerTrainingAuth.Services
{
    public class KalenderEintragsService
    {

        private readonly ApplicationDbContext _context;



        public KalenderEintragsService(ApplicationDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }




        internal async Task Set(KalenderEintragModel kEM)
        {


                var step = TimeSpan.FromDays(
                                             (kEM.SerienTerminType == 1) ? 7 :
                                             (kEM.SerienTerminType == 2) ? 28 :
                                             (kEM.SerienTerminType == 3) ? kEM.SerienTerminTagesAbstand : 1);

            Guid SerienTerminID =Guid.NewGuid();

            do
            {
                KalenderEintrag ke = new KalenderEintrag()
                {
                    Bezeichnung = kEM.Bezeichnung,
                    Anlass = kEM.Anlass,
                    Date = kEM.Date + kEM.Startzeit,
                    SerienTermin=kEM.SerienTermin,
                    SerienNummer=SerienTerminID,
                    EndDate = ((kEM.SerienTermin) ? kEM.Date : kEM.EndDate) + kEM.EndZeit
                    

                };
                if (kEM.Anlass == KalenderEventNamen.Training)
                {
                    TrainingsgruppenEintragen(kEM,ke);
                }
                else
                {
                _context.KalenderEintrags.Add(ke);

                }

            } while  (  kEM.SerienTermin && 
                        kEM.SerienTerminType >0 &&
                        kEM.SerienTerminType < 4 &&
                      ((kEM.Date += step ) <= kEM.EndDate));

           await _context.SaveChangesAsync();

        }

        internal IEnumerable<KalenderEintrag> GetTrainigstage()
        {
            KalenderEintrag SaisonEnde = GetSaisonEnde();
            KalenderEintrag SaisonStart = GetSaisonStart();

            var tt = _context.KalenderEintrags.Where(k => ((k.Anlass == KalenderEventNamen.Training) 
                                                                        || (k.Anlass == KalenderEventNamen.FlexTraining)
                                                                        || (k.Anlass == KalenderEventNamen.Heimspiel)
                                                                        || (k.Anlass == KalenderEventNamen.Auswertsspiel)
                                                                        ));

            var tt2 = tt.Where(k => k.Date >= SaisonStart.Date && k.Date <= SaisonEnde.Date).OrderByDescending(k=>k.Date);
            return tt2;
        }


        internal List<KalenderEintrag> GetTrainigstageinKW(int KW)
        {
            KalenderEintrag SaisonEnde = GetSaisonEnde();
            KalenderEintrag SaisonStart = GetSaisonStart();

            var tt = _context.KalenderEintrags.Where(k => (k.Date.GetKW()== KW) &&(k.Anlass == KalenderEventNamen.Training
                                                                                            || k.Anlass ==   KalenderEventNamen.Heimspiel
                                                                                            || k.Anlass == KalenderEventNamen.Auswertsspiel
            
                                                                                                )).ToList();
            return tt;
        }

        internal IEnumerable<KalenderEintrag> GetFlexTrainigstageinKW(int KW)
        {
            KalenderEintrag SaisonEnde = GetSaisonEnde();
            KalenderEintrag SaisonStart = GetSaisonStart();

            var tt = _context.KalenderEintrags.Where(k => (k.Date.GetKW() == KW) && (k.Anlass == KalenderEventNamen.FlexTraining)).ToList();
            return tt;
        }



        public KalenderEintrag GetSaisonStart()
        {
            var SaisonStart = _context.KalenderEintrags.LastOrDefault(s => s.Anlass == KalenderEventNamen.SaisonStart);

            if (SaisonStart == null)
            {
                SaisonStart = new KalenderEintrag();
                SaisonStart.Date = new DateTime(DateTime.Now.Year, 1, 1);
            }
            return SaisonStart;
        }


        public List<int> KalenderwochenDerSaisonMitTraining()
        {
            List<int> KWMT = new List<int>();

            KWMT = GetTrainigstage().Select(t => t.Date.GetKW()).Distinct().ToList();



            return KWMT;
        }






        public KalenderEintrag GetSaisonEnde()
        {
            KalenderEintrag SaisonEnde = _context.KalenderEintrags.LastOrDefault(s => s.Anlass == "Saison Ende");

            if (SaisonEnde == null)
            {
                SaisonEnde = new KalenderEintrag();
                SaisonEnde.Date = DateTime.Now;
            }

            return SaisonEnde;
        }

        private void TrainingsgruppenEintragen(KalenderEintragModel kEM, KalenderEintrag ke)
        {

            if (kEM.AlleGruppen)
            {
                KalenderEintrag ke1 = new KalenderEintrag();
                ke1 = ke.Copy();
                ke1.Gruppe = 0;
                ke1.AlleGruppen = true;
                _context.KalenderEintrags.Add(ke1);
            }

            else
            {

                if (kEM.Gruppe1)
                {
                    KalenderEintrag ke1 = new KalenderEintrag();
                    ke1 = ke.Copy();
                    ke1.Gruppe = 1;
                    _context.KalenderEintrags.Add(ke1);
                }

                if (kEM.Gruppe2)
                {
                    KalenderEintrag ke2 = new KalenderEintrag();
                    ke2 = ke.Copy();
                    ke2.Gruppe = 2;
                    _context.KalenderEintrags.Add(ke2);
                }
                if (kEM.Gruppe3)
                {
                    KalenderEintrag ke3 = new KalenderEintrag();
                    ke3 = ke.Copy();
                    ke3.Gruppe = 3;
                    _context.KalenderEintrags.Add(ke3);
                }
                if (kEM.Gruppe4)
                {
                    KalenderEintrag ke4 = new KalenderEintrag();
                    ke4 = ke.Copy();
                    ke4.Gruppe = 4;
                    _context.KalenderEintrags.Add(ke4);
                }
                if (kEM.Gruppe5)
                {
                    KalenderEintrag ke5 = new KalenderEintrag();
                    ke5 = ke.Copy();
                    ke5.Gruppe = 5;
                    _context.KalenderEintrags.Add(ke5);
                }
            }

        }

        internal void Delete(int id)
        {
            var ke = _context.KalenderEintrags.FirstOrDefault(k => k.ID == id);
            if (ke != null) _context.KalenderEintrags.Remove(ke);
            _context.SaveChanges();
        }

        public void DeleteSerie(int id)
        {
            var Kalenderevent = _context.KalenderEintrags.FirstOrDefault(k => k.ID ==id);
            if(Kalenderevent != null)
            {
                var kalenderEvents = _context.KalenderEintrags.Where(k => k.SerienNummer == Kalenderevent.SerienNummer);
                _context.KalenderEintrags.RemoveRange(kalenderEvents);
                _context.SaveChanges();
            }
 
        }

        public Event[] GetEvents()
        {
            List<Event> liste = new List<Event>();
            IQueryable<KalenderEintrag> kes = _context.KalenderEintrags;
            foreach(KalenderEintrag ke in kes)
            {
                Event e = new Event()
                {
                    Title = ke.Bezeichnung,
                    ID = ke.ID.ToString(),
                    Start = ke.Date,
                    End = ke.EndDate,
                    BackgroundColor = GetBackroundcolor(ke.Anlass),
                    TextColor = GetTextcolor(ke.Anlass),
                    SerienTermin=ke.SerienTermin

                };
                liste.Add(e);
            }

            return liste.ToArray();
        }

        public KalenderEintrag Get(int id)
        {
            return _context.KalenderEintrags.Find(id);
        }

        private string GetTextcolor(string anlass)
        {
           switch (anlass)
            {
                case KalenderEventNamen.Training        :  return "White";
                case KalenderEventNamen.FlexTraining    :  return "Gray";
                case KalenderEventNamen.Heimspiel       : return "Green";
                case KalenderEventNamen.Auswertsspiel   : return "Blue";
                case KalenderEventNamen.Teamsitzung     : return "DeepPink";
                case KalenderEventNamen.Geburtstag      : return "Green";
                case KalenderEventNamen.Sonstiges       : return "Blue";

                default:  return "White";
            }
        }

        private string GetBackroundcolor(string anlass)
        {
            switch (anlass)
            {
                case KalenderEventNamen.Training      : return "Red";
                case KalenderEventNamen.FlexTraining  : return "White";
                case KalenderEventNamen.Heimspiel     : return "White";
                case KalenderEventNamen.Auswertsspiel : return "Yellow";
                case KalenderEventNamen.Teamsitzung   : return "White";
                case KalenderEventNamen.Geburtstag    : return "White";
                case KalenderEventNamen.Sonstiges     : return "Yellow";   
                    
                default: return "Black";
            }
        }
    }
}
