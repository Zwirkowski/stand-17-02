﻿using Microsoft.AspNetCore.Identity;
using RangerTrainingAuth.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Services
{
    public class GruppenleiterService
    {
        private UserManager<ApplicationUser> _userManager;

        public GruppenleiterService()
        {
        }

        public   GruppenleiterService(UserManager<ApplicationUser> userManager)
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _userManager = userManager;
        }

        internal ApplicationUser Get(int gruppe)
        {
            foreach(ApplicationUser au in _userManager.Users)
            {
                if (au.Trainergruppe == gruppe) return au;
            }

            return null; 
        }

        internal bool IstGruppenleiterVakant(int gruppe)
        {
            var user = Get(gruppe);
            if (user != null) return false;
            return true;
        }
    }
}
