﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.Ergebnis;

namespace RangerTrainingAuth.Services
{
    public class ErgebnisService
    {
        public Data.ApplicationDbContext Context { get; }
        public KalenderEintragsService KalenderEintragsService { get; }

        public ErgebnisService(Data.ApplicationDbContext context, KalenderEintragsService kalenderEintragsService)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
            KalenderEintragsService = kalenderEintragsService ?? throw new ArgumentNullException(nameof(kalenderEintragsService));
        }



        internal IndexErgebnisViewModel GetErgebnisse()
        {

            IndexErgebnisViewModel  indexViewModel = new IndexErgebnisViewModel();
                                    indexViewModel.Ergebnise = new List<ErgebniseModel>();
                                    indexViewModel.GesamtErgebnise = new GesamtErgebniseModel();

          //  ErgebniseModel tagesergebnis ;

            List<int> KalenderwochenDerSaison = KalenderEintragsService.KalenderwochenDerSaisonMitTraining();


            for (int gruppe = 0; gruppe < Entities.EntityKonstanten.MaximaleGruppennummer; gruppe++)
            {

                if (indexViewModel.GesamtErgebnise.gge[gruppe] == null) indexViewModel.GesamtErgebnise.gge[gruppe] = new GruppenGesamtErgebnis();

                indexViewModel.GesamtErgebnise.gge[gruppe].Gruppe = gruppe + 1;

            }




            if (KalenderwochenDerSaison.Count() == 0) return indexViewModel;

            foreach(int KW in KalenderwochenDerSaison)
            {
                AddTraing(indexViewModel,KW);
                AddFlexTaining(indexViewModel,KW);
            }



            return indexViewModel;
        }

        private void AddFlexTaining(IndexErgebnisViewModel indexViewModel, int kW)
        {
            ErgebniseModel tagesergebnis;
           
            List<KalenderEintrag> Trainingstage = KalenderEintragsService.GetFlexTrainigstageinKW(kW).ToList();
            var teilnehmer = Context.SpielerImTraining.Where(s => s.TrainingsTag.GetKW() == kW && s.Anwesend).Count();

            if (Trainingstage.Count > 0 && teilnehmer>0)
            {
                tagesergebnis = new ErgebniseModel();

                var zeile = string.Join("oder", Trainingstage.Select(t => t.Date.ToString("dd.MM.yyyy")));

                tagesergebnis.DatumZeile = "Flextraining am " + string.Join(" oder ", Trainingstage.Select(t => t.Date.ToString("dd.MM.yyyy")));
                for (int ag = 0; ag < Entities.EntityKonstanten.MaximaleGruppennummer; ag++)
                {
                    tagesergebnis.Gruppenergebnise[ag] = new Gruppenergebnis();
                    tagesergebnis.Gruppenergebnise[ag].Anwesende = Context.SpielerImTraining.Where(s => s.TrainingsTag.GetKW() == kW
                                                                                                        && s.Anwesend
                                                                                                        && s.Gruppe == (ag + 1)
                                                                                            )
                                                                                            .Select(s => s.Spieler.ID)
                                                                                            .Distinct()
                                                                                            .Count();

                    tagesergebnis.Gruppenergebnise[ag].Gruppe = ag + 1;
                    tagesergebnis.Gruppenergebnise[ag].Punkte = tagesergebnis.Gruppenergebnise[ag].Anwesende * 20;
                    tagesergebnis.Gruppenergebnise[ag].ZusatzPunkte =
                        (tagesergebnis.Gruppenergebnise[ag].Anwesende == EntityKonstanten.MaximaleGruppenMitglieder + 1) ? 100 : 0;
                }

                Bonus2Berechnen(tagesergebnis);
                AddTagesergebnisZuGesamtergebnis(tagesergebnis, indexViewModel);
                indexViewModel.Ergebnise.Add(tagesergebnis);
            }

        }

        private void Bonus2Berechnen(ErgebniseModel tagesergebnis)
        {
             for(int gruppe =0;gruppe< EntityKonstanten.MaximaleGruppennummer; gruppe++)
            {
                int gruppenteilnehmer = tagesergebnis.Gruppenergebnise[gruppe].Anwesende;
             for(int gr =0;gr< EntityKonstanten.MaximaleGruppennummer; gr++)
                {
                    if(tagesergebnis.Gruppenergebnise[gr].Anwesende < gruppenteilnehmer)
                    {
                        tagesergebnis.Gruppenergebnise[gruppe].ZusatzPunkte2 += 20;
                    }
                }



            }
        }

        private void AddTraing(IndexErgebnisViewModel indexViewModel, int kW)
        {
            ErgebniseModel tagesergebnis;
            List<KalenderEintrag> Trainingstage = KalenderEintragsService.GetTrainigstageinKW(kW);

            foreach (KalenderEintrag kalendereintrag in Trainingstage)
            {


                int SpielerAmTraningstag = Context.SpielerImTraining.Count(s => s.TrainingsTag == kalendereintrag.Date && s.Anwesend);

                if (SpielerAmTraningstag > 0)
                {


                    tagesergebnis = new ErgebniseModel();

                    for (int ag = 0; ag < Entities.EntityKonstanten.MaximaleGruppennummer; ag++)
                    {

                        tagesergebnis.Datum = kalendereintrag.Date;
                        tagesergebnis.DatumZeile = $"{kalendereintrag.Anlass} am " + kalendereintrag.Date.ToString("dd.MM.yyyy");
                        tagesergebnis.Gruppenergebnise[ag] = new Gruppenergebnis();
                        tagesergebnis.Gruppenergebnise[ag].Anwesende = Context.SpielerImTraining.Count(s => s.TrainingsTag == kalendereintrag.Date
                                                                                                && s.Anwesend
                                                                                                && s.Gruppe == (ag + 1));

                        tagesergebnis.Gruppenergebnise[ag].Gruppe = ag + 1;
                        tagesergebnis.Gruppenergebnise[ag].Punkte = tagesergebnis.Gruppenergebnise[ag].Anwesende * 20;
                        tagesergebnis.Gruppenergebnise[ag].ZusatzPunkte =
                            (tagesergebnis.Gruppenergebnise[ag].Anwesende == EntityKonstanten.MaximaleGruppenMitglieder + 1) ? 100 : 0;
                    }

                    Bonus2Berechnen(tagesergebnis);
                    AddTagesergebnisZuGesamtergebnis(tagesergebnis, indexViewModel);
                    indexViewModel.Ergebnise.Add(tagesergebnis);

                }
            }

        }

        private void AddTagesergebnisZuGesamtergebnis(ErgebniseModel tagesergebnis, IndexErgebnisViewModel indexViewModel)
        {

            for (int gruppe = 0; gruppe < Entities.EntityKonstanten.MaximaleGruppennummer; gruppe++)
            {

                if (indexViewModel.GesamtErgebnise.gge[gruppe] == null) indexViewModel.GesamtErgebnise.gge[gruppe] = new GruppenGesamtErgebnis();

                indexViewModel.GesamtErgebnise.gge[gruppe].Gruppe = gruppe + 1;
                indexViewModel.GesamtErgebnise.gge[gruppe].Trainingsteilnahme += tagesergebnis.Gruppenergebnise[gruppe]?.Anwesende ?? 0;
                indexViewModel.GesamtErgebnise.gge[gruppe].Punkte += tagesergebnis.Gruppenergebnise[gruppe]?.Punkte ?? 0;
                indexViewModel.GesamtErgebnise.gge[gruppe].Bonus += tagesergebnis.Gruppenergebnise[gruppe]?.ZusatzPunkte ?? 0;
                indexViewModel.GesamtErgebnise.gge[gruppe].Bonus2 += tagesergebnis.Gruppenergebnise[gruppe]?.ZusatzPunkte2 ?? 0;


                indexViewModel.GesamtErgebnise.gge[gruppe].Gesamt += tagesergebnis.Gruppenergebnise[gruppe]?.ZusatzPunkte2 ?? 0;
                indexViewModel.GesamtErgebnise.gge[gruppe].Gesamt += tagesergebnis.Gruppenergebnise[gruppe]?.ZusatzPunkte ?? 0;
                indexViewModel.GesamtErgebnise.gge[gruppe].Gesamt += tagesergebnis.Gruppenergebnise[gruppe]?.Punkte ?? 0;

            }
        }

 
    }
}
