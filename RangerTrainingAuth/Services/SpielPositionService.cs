﻿using Microsoft.EntityFrameworkCore;
using RangerTrainingAuth.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Services
{
    public class SpielPositionService
    {

        private readonly Data.ApplicationDbContext _context;

        public SpielPositionService(Data.ApplicationDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public IList<Spielposition> Get(Spieler spieler)
        {
            return _context.Spieler.Include(s => s.Spielpositionen)
                                     .ThenInclude(s=>s.Spielposition)
                                   .First(s => s.ID == spieler.ID)
                                   .Spielpositionen
                                   .Select(s => s.Spielposition)
                                   .ToList();
        }



    }
}
