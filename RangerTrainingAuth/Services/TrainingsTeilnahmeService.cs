﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RangerTrainingAuth.Data;
using RangerTrainingAuth.Models.TrainingsTeilnahme;
using Microsoft.EntityFrameworkCore;
using RangerTrainingAuth.Entities;
using Microsoft.AspNetCore.Identity;

namespace RangerTrainingAuth.Services
{
    public class TrainingsTeilnahmeService
    {

        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public TrainingsTeilnahmeService(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }




        internal async Task<TrainingsTeilnahmeModel> GetAsync(DateTime? datum, System.Security.Claims.ClaimsPrincipal user)
        {
            TrainingsTeilnahmeModel uebersicht = new TrainingsTeilnahmeModel();



            var datenliste = _context.KalenderEintrags.Where(k => k.Anlass == KalenderEventNamen .Training
                                                                            || k.Anlass== KalenderEventNamen.FlexTraining
                                                                            || k.Anlass== KalenderEventNamen.Heimspiel
                                                                            || k.Anlass== KalenderEventNamen.Auswertsspiel
                                                                            ).OrderByDescending(k=>k.Date);
            List<DateTime> tt = new List<DateTime>();
            foreach (KalenderEintrag ke in datenliste)
            {
                // keine zukünftigen Termine
                if (ke.Date <= DateTime.Now) tt.Add(ke.Date);
            }
            uebersicht.Trainingstage = tt.ToArray();
            var SaisonEnde = _context.KalenderEintrags.LastOrDefault(s => s.Anlass == KalenderEventNamen.SaisonEnde);

            uebersicht.SaisonEnde = (SaisonEnde != null) ? SaisonEnde.Date : new DateTime(DateTime.Now.Year,12,31);
            var SaisonStart = _context.KalenderEintrags.LastOrDefault(s => s.Anlass == KalenderEventNamen.SaisonStart);
            uebersicht.SaisonStart = (SaisonStart != null) ? SaisonStart.Date : new DateTime(DateTime.Now.Year, 1, 1);



            uebersicht.Trainingstag = datum;
            uebersicht.TrainingstagString = (datum == null) ? "" : ((DateTime)datum).ToString("dd.MM.yyyy");

            uebersicht.SpielerImTrainings = await GetTrainingsgruppeAsync(datum,user);



            return uebersicht;
        }

        public async Task<int> GetGruppeImTrainingAsync(DateTime datum, int spielerID)
        {

            var all = _context.SpielerImTraining.Where(d => d.TrainingsTag.Date == new DateTime(2020,1,6)).ToArray();
            
            var result = await _context.SpielerImTraining.FirstOrDefaultAsync(sit => sit.Anwesend
                                                                                  && sit.TrainingsTag == datum
                                                                                  && sit.Spieler.ID == spielerID);

            return result?.Gruppe ?? 0;
        }


        private async Task< SpielerImTraining[]> GetTrainingsgruppeAsync(DateTime? datum, System.Security.Claims.ClaimsPrincipal user)
        {
            var curUser = await userManager.GetUserAsync(user);
            var gruppe = curUser.Trainergruppe;

            if (datum != null)
            {
                var SpielerimTraining= _context.SpielerImTraining.Include(s=>s.Spieler )  
                                                    .Where(s => s.TrainingsTag == datum)
                                                    .ToArray();

               
                foreach(SpielerImTraining st in SpielerimTraining)
                {
                    st.Eneabled = !(st.Gruppe == gruppe);
                }



                return SpielerimTraining.OrderBy(s => s.Gruppe).ToArray(); ;

            }

            return null;
        }

        internal async Task SetTeilnahmeAsync(TrainingsTeilnahmeModel submitModel)
        {
            if (submitModel.SpielerImTrainings?.Count() >0)
            {
                foreach (SpielerImTraining sit in submitModel.SpielerImTrainings)
                {
                    SpielerImTraining spielerindb = _context.SpielerImTraining.FirstOrDefault(s => s.Spieler.Vorname == sit.Spieler.Vorname
                                                                                                    && s.Spieler.Nachname == sit.Spieler.Nachname
                                                                                                    && s.TrainingsTag == submitModel.Trainingstag);
                    if (spielerindb == null) throw new ArgumentNullException(nameof(spielerindb));

                    if (!sit.Eneabled)
                    {

                        spielerindb.Anwesend = sit.Anwesend;
                       await _context.SaveChangesAsync();
                    }

                } 
            }
             
        }
       
    }
}
