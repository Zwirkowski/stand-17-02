﻿using Microsoft.AspNetCore.Identity;
using RangerTrainingAuth.Data;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.Übersicht;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Services
{
    public class ÜbersichtsService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly KalenderEintragsService _kalenderEintragsService;

        public ÜbersichtsService(ApplicationDbContext context, UserManager<ApplicationUser> userManager, KalenderEintragsService kalenderEintragsService)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            if (kalenderEintragsService == null)
                throw new ArgumentNullException(nameof(kalenderEintragsService));

            _context = context;
            _userManager = userManager;
            _kalenderEintragsService = kalenderEintragsService;
        }

        internal List<TrainingsTag> GetSpielerAnTrainingstag(List<TrainingsTag> trainingstage)
        {
            var ListeTraningstage = _kalenderEintragsService.GetTrainigstage().ToArray();

            foreach(KalenderEintrag ke in ListeTraningstage)
            {
                TrainingsTag traingstag = new TrainingsTag();
                traingstag.Datum = ke.Date;
                trainingstage.Add(traingstag);
            }


            return trainingstage;
        }

        internal List<Spieler> GetSpieler()
        {
            var sp= _context.SpielerImTraining.Select(s=>s.Spieler).Distinct();

            return sp.ToList();
        }
    }
}
