﻿using RangerTrainingAuth.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Services
{
    public class SpielerService
    {
        private readonly Data.ApplicationDbContext _context;

        public SpielerService(Data.ApplicationDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public Spieler Get(int id)
        {
            return _context.Spieler.FirstOrDefault(s => s.ID == id);
        }

        public async Task UpdateAsync(Spieler spieler)
        {
            _context.Update(spieler);
          await  _context.SaveChangesAsync();
        }
    }
}
