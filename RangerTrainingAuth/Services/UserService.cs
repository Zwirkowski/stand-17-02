﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RangerTrainingAuth.Data;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.ClearUser;
using RangerTrainingAuth.Models.User;
using RangerTrainingAuth.Views.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Services
{
    public class UserService
    {
        private readonly ApplicationDbContext _applicationDb;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserService(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager)
        {
            if (applicationDbContext == null)
                throw new ArgumentNullException(nameof(applicationDbContext));

            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _applicationDb = applicationDbContext;
            _userManager = userManager;
        }

        internal List<UserToClear> GetUserToClear()
        {
            List<UserToClear> lcvm = new List<UserToClear>();
            IQueryable<ApplicationUser> users = _userManager.Users.Include(s => s.Spieler).ThenInclude(p => p.Spielpositionen);

            foreach (ApplicationUser au in users)
            {
                UserToClear clvm = new UserToClear();
                clvm.ID = au.Id;
                clvm.Name = au.UserName;
                clvm.Spieler = au.Spieler;
                lcvm.Add(clvm);
            }
            return lcvm;
        }

        internal string[] GetUserListe(string namenfilter)
        {

            List<string> x = new List<string>();

            if (namenfilter != null)
            {
                var selectedUser = _applicationDb.Users.Where(u => u.UserName.Contains(namenfilter));
                foreach (ApplicationUser au in selectedUser)
                {
                    x.Add(au.UserName);
                }
            }

            else
            {
                var selectedUser = _applicationDb.Users.OrderBy(s => s.UserName);
                foreach (ApplicationUser au in selectedUser)
                {
                    x.Add(au.UserName);
                }
            }

            return x.ToArray();
        }

        internal EditUserViewModel GetUserDaten(string username)
        {
            EditUserViewModel editUserViewModel = new EditUserViewModel();

            var ApplikationsUser = _userManager.Users.FirstOrDefault(u => u.UserName == username);

            if (ApplikationsUser == null) return null;
            editUserViewModel.Name = ApplikationsUser.UserName;
            editUserViewModel.ID = ApplikationsUser.Id; ;

            Spieler spieler = _applicationDb.Spieler.Include(s => s.Spielpositionen).ThenInclude(s => s.Spielposition).FirstOrDefault(s => s.User == ApplikationsUser);

            //var y = spieler.Spielposition.Name;

            if (spieler != null)
            {
                editUserViewModel.Vorname = spieler.Vorname;
                editUserViewModel.Nachname = spieler.Nachname;
                editUserViewModel.Geburtstag = spieler.Geburtstag;
                editUserViewModel.Klasse = spieler.Klasse;
                editUserViewModel.SpielerNummer = spieler.SpielerNummer;
                editUserViewModel.Spielpositionen = GetUserSpielposition(spieler);
                editUserViewModel.Spieler = spieler.SpielerNummer > 0;

            }
            return editUserViewModel;
        }


        internal async Task ClearUserAsync(string name)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(name).ConfigureAwait(false);
            if (user != null)

            {
                Spieler spieler = await _applicationDb.Spieler.FirstOrDefaultAsync(s => s.User == user).ConfigureAwait(false);

                spieler.User = null;
                spieler.Deaktiviert = true;
                await _applicationDb.SaveChangesAsync().ConfigureAwait(false);

                await _userManager.DeleteAsync(user).ConfigureAwait(false);


            }
        }

        public void SetUserDaten(EditUserViewModel userdaten)
        {
            Spieler spieler = _applicationDb.Spieler.Include(s => s.Spielpositionen).FirstOrDefault(s => s.User.Id == userdaten.ID);
            if (spieler == null)
            {
                spieler = new Spieler();
                spieler.UserId = userdaten.ID;
                _applicationDb.Spieler.Add(spieler);

            }


            spieler.Nachname = userdaten.Nachname;
            spieler.Vorname = userdaten.Vorname;
            spieler.Geburtstag = userdaten.Geburtstag;
            spieler.SpielerNummer = userdaten.Spieler ? userdaten.SpielerNummer : 0;

            List<SpielerSpielposition> ssp = new List<SpielerSpielposition>();

            foreach (UserSpielposition usp in userdaten.Spielpositionen)
            {
                Spielposition sp = _applicationDb.Spielposition.FirstOrDefault(s => s.ID == usp.ID);

                if (usp.Auswahl) ssp.Add(new SpielerSpielposition() { Spieler = spieler, Spielposition = sp });
            }

            //_applicationDb.RemoveRange(spieler.Spielpositionen);
            //spieler.Spielpositionen.Clear();
            //_applicationDb.SaveChanges();

            spieler.Spielpositionen = ssp;

            _applicationDb.SaveChanges();
        }

        private List<UserSpielposition> GetUserSpielposition(Spieler spieler)
        {
            var sp = _applicationDb.Spielposition;

            List<UserSpielposition> Lusp = new List<UserSpielposition>();

            foreach (Spielposition spielpos in sp)
            {
                UserSpielposition usp = new UserSpielposition()
                {
                    ID = spielpos.ID,
                    Name = spielpos.Name,
                    Auswahl = spieler.Spielpositionen.Exists(s => s.Spielposition.ID == spielpos.ID)

                };
                Lusp.Add(usp);

            }
            return Lusp;
        }
        internal async Task ClearAllUserAsync()
        {
            removeSpielerImTraining();
            await RemoveUserAync().ConfigureAwait(false);
            removeSpieler();


        }

        private async Task RemoveUserAync()
        {
            var users = _userManager.Users.Where(u => u.Id != null);

            foreach (ApplicationUser au in users.ToArray())
            {
                {
                    Spieler spieler = _applicationDb.Spieler.FirstOrDefault(s => s.User == au);

                    spieler.User = null;
                    spieler.Deaktiviert = true;
                    _applicationDb.SaveChanges();

                    await _userManager.DeleteAsync(au).ConfigureAwait(false);


                }

            }

        }

        private void removeSpieler()
        {
            var spieler = _applicationDb.Spieler;
            foreach (Spieler sp in spieler)
            {
                _applicationDb.Spieler.Remove(sp);
                _applicationDb.SaveChanges();
            }
        }

        private void removeSpielerImTraining()
        {
            IEnumerable<SpielerImTraining> sit = _applicationDb.SpielerImTraining.Where(s => s.Spieler != null);

            foreach (SpielerImTraining spi in sit)
            {
                _applicationDb.SpielerImTraining.Remove(spi);
            }
            _applicationDb.SaveChangesAsync();
        }


    }
}
