﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using RangerTrainingAuth.Entities;

namespace RangerTrainingAuth.Services
{
    public class SteuerungsService
    {
        private readonly Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public SteuerungsService(Data.ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public Steuerungsdaten Get()
        {
            var sd = _context.SteuerungsDaten.FirstOrDefault();
            if (sd != null) return sd;

            sd = new Steuerungsdaten();
            _context.SteuerungsDaten.Add(sd);
            _context.SaveChanges();
            return sd;
        }

        public void Set(Steuerungsdaten steuerungsdaten)
        {
            if (_context.SteuerungsDaten.Any() && steuerungsdaten.ID > 0)
            {
                _context.SteuerungsDaten.Update(steuerungsdaten);

            } else
            {
                _context.SteuerungsDaten.RemoveRange(_context.SteuerungsDaten.ToArray());
                _context.SteuerungsDaten.Add(steuerungsdaten);
            }

            _context.SaveChanges();
        }

        internal async Task SpielerInWocheEintragenAsync(int kalenderwoche)

            
        {
           IEnumerable<Spieler>  Trainingsgruppen = _context.Spieler
                                                            .Include(s => s.Spielpositionen)
                                                            .Include(s => s.User)
                                                            .Where(s => s.Trainingsgruppe > 0)
                                                            .OrderBy(s => s.Trainingsgruppe);
           IEnumerable < KalenderEintrag > kalenderListe = _context.KalenderEintrags.Where(k => k.Date.GetKW() == kalenderwoche);

            foreach(KalenderEintrag ke in kalenderListe)
            {
                foreach (Spieler sp in Trainingsgruppen)

                    if (sp.UserId != null)
                    {

                        {
                            SpielerImTraining sit = new SpielerImTraining();
                            sit.Spieler = sp;
                            sit.Gruppe = sp.Trainingsgruppe;
                            sit.IstGruppenleiter = await userManager.IsInRoleAsync(sp.User, Data.Rolename.Gruppenleiter);
                            sit.Eneabled = false;
                            sit.TrainingsTag = ke.Date;


                            var spielerInListe = _context.SpielerImTraining.FirstOrDefault(s => (s.Spieler == sp) && (s.TrainingsTag == ke.Date));

                            if (spielerInListe == null)
                            {
                                _context.SpielerImTraining.Add(sit);
                                await _context.SaveChangesAsync();
                            }
                        }

                    }

            }
            return ;
        }
    }
}
