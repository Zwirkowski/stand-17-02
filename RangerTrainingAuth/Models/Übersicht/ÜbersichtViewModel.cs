﻿using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.Übersicht
{
    public class ÜbersichtViewModel
    {
        public List<Spieler> Spieler { get; set; } = new List<Spieler>();
        public List<TrainingsTag> Trainingstage { get; set; } = new List<TrainingsTag>();
        public List<TableRow> TableRows { get; set; } = new List<TableRow>();
    }

    public class TableRow
    {
        public DateTime Date { get; set; }

        public IEnumerable<string> Spielernamen => _data.Keys;

        private readonly Dictionary<string, TableCell> _data = new Dictionary<string, TableCell>();

        public TableCell this[string spielerName]
        {
            get {
                if (_data.TryGetValue(spielerName, out TableCell value))
                {
                    return value;
                }

                return new TableCell();
            }
            set
            {
                _data[spielerName] = value;
            }
        }
    }

    public class TableCell
    {
        public int Gruppe { get; set; }
    }

    public class TrainingsTag
    {
       public DateTime Datum { get; set; }
       public List<SpielerImTraining> SpielerImTrainingListe { get; set; }

    }

    public class SpielerImTraining
    {
        public int SpielerID { get; set; }
        public bool Anwesend { get; set; }
    }

}
