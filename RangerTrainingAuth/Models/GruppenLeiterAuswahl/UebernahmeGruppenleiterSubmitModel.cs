﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RangerTrainingAuth.Models.GruppenLeiterAuswahl
{
    public class UebernahmeGruppenleiterSubmitModel
    {
        [Required]
        public IEnumerable<bool> Auswahl { get; set; }

        [Required]
        public IEnumerable<string> Namen { get; set; }
    }
}
