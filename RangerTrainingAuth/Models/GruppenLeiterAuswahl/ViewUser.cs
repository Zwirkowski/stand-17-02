﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.GruppenLeiterAuswahl
{
    public class ViewUser
    {
        public bool Admin { get; set; }
        public string Name { get; set; }
        public int Gruppe { get; set; }
    }
}
