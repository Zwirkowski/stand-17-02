﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.GruppenLeiterAuswahl
{
    public class IndexViewModel
    {
        public ViewUser[] User { get; set; }

        public bool UebernahmeErfolgreich { get; set; }
    }
}
