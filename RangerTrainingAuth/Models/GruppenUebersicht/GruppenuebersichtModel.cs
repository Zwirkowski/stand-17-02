﻿using RangerTrainingAuth.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.GruppenUebersicht
{
    public class GruppenuebersichtModel
    {
        public List<SpielerInGruppe> Trainingsgruppen { get; set; } = new List<SpielerInGruppe>();

    }

    public class SpielerInGruppe
    {
        public int Gruppe { get; set; }
        public string Name { get; set; }
        public string Gruppenleiter { get; set; }
    }
}
