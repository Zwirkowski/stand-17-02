﻿using RangerTrainingAuth.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.TrainingsTeilnahme
{
    public class TrainingsTeilnahmeModel
    {
        [Required, MaxLength(40), MinLength(4)]
        public string TrainingstagString { get; set; }
        public DateTime? Trainingstag { get; set; }
        public DateTime SaisonStart { get; set; }
        public string SaisonStartString => SaisonStart.ToString("dd.MM.yyyy");
        public DateTime SaisonEnde { get; set; }
        public string SaisonEndeString => SaisonEnde.ToString("dd.MM.yyyy");
        // public int TrainingsGruppe { get; set; }
        public DateTime[] Trainingstage { get; set; }

        public SpielerImTraining[] SpielerImTrainings { get; set; }
    }
}
