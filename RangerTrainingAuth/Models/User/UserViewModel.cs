﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.User
{
    public class UserViewModel
    {
        public string[] UserListe { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Namenfilter { get; set; }
        public DateTime Geburtstag { get; set; }
        public int SpielerNummer { get; set; }
        public string Klasse { get; set; }
        public string Spielposition { get; set; }

    }
}
