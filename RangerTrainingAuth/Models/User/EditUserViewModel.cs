﻿using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Views.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.User
{
    public class EditUserViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; } = "?";
        public bool Spieler { get; set; }
        public int SpielerNummer { get; set; } = 0;
        public string Vorname { get; set; } ="?";
        public string Nachname { get; set; } = "?";
        public DateTime Geburtstag { get; set; }
        public Spielerklasse Klasse { get; set; } = Spielerklasse.Fan;
        public List<UserSpielposition> Spielpositionen { get; set; } = new List<UserSpielposition>();
    }
}
