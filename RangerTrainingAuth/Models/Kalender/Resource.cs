﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.Kalender
{
    public class Resource
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string EventBackgroundColor { get; set; }
        public string EventBorderColor { get; set; }
        public string EventTextColor { get; set; }
    }
}
