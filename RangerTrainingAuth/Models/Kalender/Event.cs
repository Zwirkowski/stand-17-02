﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.Kalender
{
    public class Event
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string BackgroundColor { get; set; }
        public string TextColor { get; set; }
        public string ResourceId { get; set; }
        public bool SerienTermin { get; set; }
    }
}
