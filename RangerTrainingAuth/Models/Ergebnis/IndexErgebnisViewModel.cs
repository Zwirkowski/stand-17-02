﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.Ergebnis
{
    public class IndexErgebnisViewModel
    {
            public List<ErgebniseModel> Ergebnise { get; set; }
            public GesamtErgebniseModel GesamtErgebnise { get; set; }

    }
}
