﻿namespace RangerTrainingAuth.Models.Ergebnis
{
    public class GesamtErgebniseModel
    {
        public GruppenGesamtErgebnis[] gge { get; set; } = new GruppenGesamtErgebnis[Entities.EntityKonstanten.MaximaleGruppennummer];
    }

    public class GruppenGesamtErgebnis
    {
        private int gesamt;

        public int Trainingsteilnahme { get; set; }
        public int Gruppe { get; set; }
        public int Bonus { get; set; }
        public int Bonus2 { get; set; }
        public int Punkte { get; set; }
        public int Gesamt { get => gesamt; set => gesamt = value; }
    }
}