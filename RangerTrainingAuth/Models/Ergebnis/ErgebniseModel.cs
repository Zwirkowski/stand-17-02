﻿using RangerTrainingAuth.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.Ergebnis
{
    public class ErgebniseModel

    {
        public DateTime Datum { get; set; }
        public bool IstFlexTraining { get; set; }
        public String DatumZeile { get; set; }
        public Gruppenergebnis[] Gruppenergebnise { get; set; } = new Gruppenergebnis[EntityKonstanten.MaximaleGruppennummer];   
    }

    public class Gruppenergebnis

    {
        public int Anwesende { get; set; }
        public bool AnwesendeFT { get; set; }
        public int Gruppe { get; set; }
        public int Punkte { get; set; }
        public int ZusatzPunkte { get; set; }
        public int ZusatzPunkte2 { get; set; }
    }
}
