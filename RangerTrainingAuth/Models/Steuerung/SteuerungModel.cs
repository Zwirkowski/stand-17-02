﻿using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.Steuerung
{
    public class SteuerungModel
    {


        public DateTime AktuellesDatum { get; set; } = DateTime.Now;


        public int Kalenderwoche { get; set; } = DateTime.Now.GetKW();
        public Steuerungsdaten Steuerungsdaten { get; set; }
        public string Gruppentext  { get; set; }


    }
}
