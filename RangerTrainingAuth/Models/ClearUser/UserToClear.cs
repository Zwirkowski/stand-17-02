﻿using RangerTrainingAuth.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.ClearUser
{
    public class UserToClear
    {
        public string ID { get; set; }
        public string Name { get; set; } = "?";
        public Spieler Spieler { get; set; }

        public bool Selected { get; set; }
    }
}
