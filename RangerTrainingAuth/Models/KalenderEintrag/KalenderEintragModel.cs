﻿using RangerTrainingAuth.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.KalenderEintrag
{
    public class KalenderEintragModel
    {
        public String Bezeichnung { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now;
        public TimeSpan Startzeit { get; set; } 
        public TimeSpan EndZeit { get; set; }

        public string Anlass { get; set; } = KalenderEventNamen.Training;
        public bool SerienTermin { get; set; } = false;
        public int SerienTerminType { get; set; } 
        public int SerienTerminTagesAbstand { get; set; } = 0;
        public bool AlleGruppen { get; set; } = true;
        public bool Gruppe1 { get; set; } = false;
        public bool Gruppe2 { get; set; } = false;
        public bool Gruppe3 { get; set; } = false;
        public bool Gruppe4 { get; set; } = false;
        public bool Gruppe5 { get; set; } = false;

    }
}
