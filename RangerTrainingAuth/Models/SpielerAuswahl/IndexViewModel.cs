﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.SpielerAuswahl
{
    public class IndexViewModel
    {
        public ViewSpieler[] Spieler { get; set; }
        public int Gruppe { get; set; }
        public bool AuswahlNichtMöglich { get; set; }
    }
}
