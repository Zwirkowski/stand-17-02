﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Models.SpielerAuswahl
{
    public class ViewSpieler
    {
        public bool IstGruppenMitglied { get; set; }
        public string Name { get; set; }
        public int ID { get; set; }
        public int Nummer { get; set; }
        public List<string> Positionen { get; set; }
    }
}
