﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Models.TrainingsTeilnahme;
using RangerTrainingAuth.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RangerTrainingAuth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingsTeilnahmeController : Controller
    {
        private readonly TrainingsTeilnahmeService trainingsTeilnahmeService;

        public TrainingsTeilnahmeController(TrainingsTeilnahmeService trainingsTeilnahmeService)
        {
            if (trainingsTeilnahmeService == null)
                throw new ArgumentNullException(nameof(trainingsTeilnahmeService));

            this.trainingsTeilnahmeService = trainingsTeilnahmeService;
        }



        public async Task<IActionResult> Index(DateTime? datum = null)
        {

           
            TrainingsTeilnahmeModel viewmodel = await trainingsTeilnahmeService.GetAsync(datum, User);


            return View(viewmodel);
        }

        [HttpPost]
        public async Task<IActionResult> UebernahmeTeilnahmeImTraining([FromForm]TrainingsTeilnahmeModel submitModel)
        {
            if (ModelState.IsValid)
            {

            }

            await trainingsTeilnahmeService.SetTeilnahmeAsync(submitModel);


            return View();
        }
    }
}


