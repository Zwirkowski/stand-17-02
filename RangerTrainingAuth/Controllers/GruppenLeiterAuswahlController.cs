﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RangerTrainingAuth.Data;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.GruppenLeiterAuswahl;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
    [Authorize(Roles= Rolename.Admin )]
   
    public class GruppenLeiterAuswahlController : Controller
    {
        private  UserManager<ApplicationUser> _userManager;
        private readonly GruppenleiterService gruppenleiterService;

        public GruppenLeiterAuswahlController(UserManager<ApplicationUser> userManager, GruppenleiterService gruppenleiterService )
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _userManager = userManager;
            this.gruppenleiterService = gruppenleiterService ?? throw new ArgumentNullException(nameof(gruppenleiterService));
        }


        public  async Task<IActionResult> Index([FromQuery]bool success = false)
        {
            IndexViewModel viewmodel = new IndexViewModel()
            {
                UebernahmeErfolgreich = success
            };
            List<ViewUser> user = new List<ViewUser>();

            foreach (ApplicationUser appuser in _userManager.Users )
            {
                //if (appuser.Spieler != null)
                {

                    user.Add(new ViewUser()
                    {
                        Admin = await _userManager.IsInRoleAsync(appuser, Rolename.Admin).ConfigureAwait(false),
                        Gruppe = appuser.Trainergruppe,
                        Name = appuser.UserName
                    });
                }
            }

            viewmodel.User = user.OrderByDescending (u=>u.Gruppe).ToArray();

            return View(viewmodel);
        }

        [HttpPost]
        public async Task<IActionResult> UebernahmeGruppenleiter([FromForm]IndexViewModel submitModel)
        {
            if (submitModel is null)
            {
                throw new ArgumentNullException(nameof(submitModel));
            }

            DateTime StartZeit = DateTime.Now;
            //Hier die Funktion einfügen deren Zeit gemessen werden soll
           


            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(null, "Ungültige Daten.");

                return View(nameof(Index), submitModel);
            }
            else if (submitModel.User.Count(u => u.Gruppe >0) > EntityKonstanten.MaximaleGruppennummer)
            {
                ModelState.AddModelError(nameof(submitModel.User), $"Es dürfen nur max. {EntityKonstanten.MaximaleGruppennummer} Gruppenleiter gewählt werden.");

                return View(nameof(Index), submitModel);
            }
            else
            {
                //TODO diese routine muss schneller werden
                foreach (ViewUser vu in submitModel.User)
                {

                  

                    ApplicationUser user = await _userManager.Users.Include(u => u.Spieler)
                                                           .FirstOrDefaultAsync(u => u.UserName == vu.Name).ConfigureAwait(false);

                    if (await _userManager.IsInRoleAsync(user, Rolename.Gruppenleiter).ConfigureAwait(false) && vu.Gruppe ==0)
                    {
                        await _userManager.RemoveFromRoleAsync(user, Rolename.Gruppenleiter).ConfigureAwait(false);
                        if (user.Spieler != null)
                        {
                            user.Spieler.Trainingsgruppe = 0;
                        }
                        user.Trainergruppe = 0;
                        
                    }
                    else if (!await _userManager.IsInRoleAsync(user, Rolename.Gruppenleiter) && vu.Gruppe >0)
                    {
                        bool gruppefree = gruppenleiterService.IstGruppenleiterVakant(vu.Gruppe);
                        if (gruppefree)
                        {
                            await _userManager.AddToRoleAsync(user, Rolename.Gruppenleiter);
                            if (user.Spieler != null)
                            {
                                user.Spieler.Trainingsgruppe = vu.Gruppe;
                            }
                        }
                        else
                        {
                            ModelState.AddModelError(nameof(submitModel.User), $"Gruppenleiter ist schon gewählt.");

                            return View(nameof(Index), submitModel);
                        }
                    }

                    else if (await _userManager.IsInRoleAsync(user, Rolename.Gruppenleiter) && vu.Gruppe > 0)
                    {
                         
                        if (user.Spieler != null)
                        {
                            user.Spieler.Trainingsgruppe = vu.Gruppe;
                        }
                    }






                    user.Trainergruppe = vu.Gruppe;


                    if((await _userManager.IsInRoleAsync(user, Rolename.Admin)) && !vu.Admin)
                    {
                        await _userManager.RemoveFromRoleAsync(user, Rolename.Admin);

                    }
                    else if((!await _userManager.IsInRoleAsync(user, Rolename.Admin)) && vu.Admin)
                    {
                        await _userManager.AddToRoleAsync(user, Rolename.Admin).ConfigureAwait(false);
                    }




                    await _userManager.UpdateAsync(user);
                var rolle =await  _userManager.GetRolesAsync(user);
                }


                await HttpContext.RefreshAuthCookieAsync();

                //return View();
                 TimeSpan GemessendeZeit = DateTime.Now - StartZeit;
                Debug.WriteLine($"Zeit für die Verarbeitung der Gruppenleiter {GemessendeZeit}");


                return RedirectToAction(nameof(Index), new { Success = true });




            }
        }

    }
}