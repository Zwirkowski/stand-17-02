﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.Steuerung;

namespace RangerTrainingAuth.Controllers
{
    public class SteuerungController : Controller
    {
        private readonly Services.SteuerungsService _steuerungsService;
        private readonly UserManager<ApplicationUser> userManager;

        public SteuerungController(Services.SteuerungsService  steuerungsService, UserManager<ApplicationUser> userManager)
        {
            if (steuerungsService == null)
                throw new ArgumentNullException(nameof(steuerungsService));

            _steuerungsService = steuerungsService;
            this.userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        // GET: Steurungs
        public ActionResult Index()
        {
            SteuerungModel sm =  new SteuerungModel();
            sm.Steuerungsdaten = _steuerungsService.Get();
            sm.Gruppentext = (sm.Steuerungsdaten.TradingFürGruppeErlaubt == 0)
                            ? "Trading nicht erlaubt"
                            : $"Trading für Gruppe {sm.Steuerungsdaten.TradingFürGruppeErlaubt} erlaubt";
            return View(sm);
        }


        [HttpPost]
        public async Task<ActionResult> SpielerinWocheAsync(int kalenderwoche)
        {
           await _steuerungsService.SpielerInWocheEintragenAsync(kalenderwoche);
            return Redirect("index");
        }

        [HttpPost]
        public ActionResult Trading()
        {
            SteuerungModel sm = new SteuerungModel();
            sm.Steuerungsdaten = _steuerungsService.Get();
            sm.Steuerungsdaten.TradingFürGruppeErlaubt = (sm.Steuerungsdaten.TradingFürGruppeErlaubt < EntityKonstanten.MaximaleGruppennummer) 
                                                            ?sm.Steuerungsdaten.TradingFürGruppeErlaubt+1
                                                            :0 ;
            _steuerungsService.Set(sm.Steuerungsdaten);

            sm.Gruppentext = (sm.Steuerungsdaten.TradingFürGruppeErlaubt == 0) 
                            ? "Trading nicht erlaubt" 
                            : $"Trading für Gruppe {sm.Steuerungsdaten.TradingFürGruppeErlaubt} erlaubt";
            return View("index", sm);
        }


        // GET: Steurungs/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Steurungs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Steurungs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Steurungs/Edit/5
        public ActionResult Edit(int id)
        {

            return View();
        }

        // POST: Steurungs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                 

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Steurungs/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Steurungs/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                 

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}