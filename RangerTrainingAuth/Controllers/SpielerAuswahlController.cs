﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RangerTrainingAuth.Data;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.SpielerAuswahl;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
 
    [Authorize(Roles = Rolename.Gruppenleiter)]

    public class SpielerAuswahlController : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        private readonly SpielerService _spielerservice;
        private readonly SteuerungsService steuerungsService;
        private readonly SpielPositionService _spielPosition;

        public SpielerAuswahlController(UserManager<ApplicationUser> userManager, 
                                        SpielerService spielerservice,
                                        Services.SteuerungsService steuerungsService,
                                        SpielPositionService spielPosition)
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            if (spielPosition == null)
                throw new ArgumentNullException(nameof(spielPosition));

            _userManager = userManager;
            _spielerservice = spielerservice ?? throw new ArgumentNullException(nameof(spielerservice));
            this.steuerungsService = steuerungsService ?? throw new ArgumentNullException(nameof(steuerungsService));
            _spielPosition = spielPosition;
        }


        public async Task<IActionResult> Index()
        {
            IndexViewModel viewmodel = new IndexViewModel();

            
            List<ViewSpieler> user = new List<ViewSpieler>();
            var curUser = await _userManager.GetUserAsync(User);
            var gruppe = curUser.Trainergruppe;

            foreach (ApplicationUser appuser in _userManager.Users.Include(u => u.Spieler).Where(u => u.Trainergruppe == 0
                                                                                                    && (u.Spieler.Trainingsgruppe == 0
                                                                                                    || u.Spieler.Trainingsgruppe == gruppe)))
            {
                user.Add(new ViewSpieler()
                {

                    IstGruppenMitglied = (gruppe == appuser.Spieler.Trainingsgruppe),
                    Name = appuser.UserName,
                    ID = appuser.Spieler.ID,
                    Nummer = appuser.Spieler.SpielerNummer,
                    Positionen = _spielPosition.Get(appuser.Spieler).Select(s=>s.Name).ToList()


                });
            }



            var steurung = steuerungsService.Get();

            viewmodel.Spieler = user.OrderByDescending(s=>s.IstGruppenMitglied).ThenBy(s=>s.Name).   ToArray();
            viewmodel.Gruppe = gruppe;
            viewmodel.AuswahlNichtMöglich = (gruppe != steurung.TradingFürGruppeErlaubt);



            return View(viewmodel);
        }

        [HttpPost]
        public async Task<IActionResult> UebernahmeSpieler([FromForm]IndexViewModel submitModel)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(null, "Ungültige Daten.");

                return View(nameof(Index), submitModel);
            }
            else if (submitModel.Spieler.Count(u => u.IstGruppenMitglied) > EntityKonstanten.MaximaleGruppenMitglieder)
            {
                ModelState.AddModelError(nameof(submitModel.Spieler), $"Es dürfen nur max. {EntityKonstanten.MaximaleGruppenMitglieder} Spieler gewählt werden.");

                return View(nameof(Index), submitModel);
            }
            else
            {
                var trainingsgruppe = (await _userManager.GetUserAsync(User)).Trainergruppe;

                foreach (ViewSpieler vu in submitModel.Spieler)
                {
                    Spieler spieler = _spielerservice.Get(vu.ID);

                    if (vu.IstGruppenMitglied) spieler.Trainingsgruppe = trainingsgruppe;
                    else spieler.Trainingsgruppe = 0;

                    await _spielerservice.UpdateAsync(spieler);


                }

                return View();
            }
        }

    }
}