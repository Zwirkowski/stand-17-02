﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Data;
using RangerTrainingAuth.Models.KalenderEintrag;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
    [Authorize(Roles = Rolename.Admin)]
    public class KalenderEintragController : Controller
    {
        private KalenderEintragsService _kalenderEintragsService;

        public KalenderEintragController(KalenderEintragsService kalenderEintragsService)
        {
            _kalenderEintragsService = kalenderEintragsService ?? throw new ArgumentNullException(nameof(kalenderEintragsService));
        }

        public IActionResult Index()
        {
            return View(new   KalenderEintragModel());
        }

        [HttpGet]
        public IActionResult Delete([FromQuery]int id)
        {
            var eintrag = _kalenderEintragsService.Get(id);

            if (eintrag == null)
            {
                return NotFound();
            }
            else
            {
                return View(eintrag);
            }
        }

        [HttpPost]
        public IActionResult DeleteEintrag([FromForm]int id)
        {
            var eintrag = _kalenderEintragsService.Get(id);

            if (eintrag == null)
            {
                return NotFound();
            }
            else
            {
                _kalenderEintragsService.Delete(id);

                return RedirectToAction("Index", "Kalender");
            }
        }

        [HttpPost]
        public IActionResult DeleteSerienEintrag([FromForm]int id, [FromForm]int clearType)
        {
            var eintrag = _kalenderEintragsService.Get(id);

           

            if (eintrag == null)
            {
                return NotFound();
            }
            else if(clearType == 1)
            {
            
                _kalenderEintragsService.DeleteSerie(id);

                return RedirectToAction("Index", "Kalender");
            }
            return BadRequest();


        }

        [HttpPost]
        public async Task<IActionResult> KalendereintragUebernahme([FromForm]KalenderEintragModel KalenderEintragModel)
        {

           await _kalenderEintragsService.Set(KalenderEintragModel);

            return View();
        }

    }
}