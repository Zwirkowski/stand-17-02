﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Models.ClearUser;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
    public class ClearUserController : Controller
    {
        private readonly UserService userService;

        public ClearUserController(UserService userService)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        // GET: ClearUser
        public ActionResult Index()
        {
             ClearUserViewModel vi = new  ClearUserViewModel();
            vi.UserToClearListe = userService.GetUserToClear();
            return View(vi );
        }

        // GET: ClearUser/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ClearUser/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ClearUser/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ClearUser/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ClearUser/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        


        // POST: ClearUser/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(ClearUserViewModel collection)
        {
            if (collection is null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            var count = collection.UserToClearListe.Where(u => u.Selected).Count();

            if(count ==1)
            {
                UserToClear selectetUser = collection.UserToClearListe.FirstOrDefault(u => u.Selected);



                if(selectetUser!= null && selectetUser.Name == "Georg Zwirkowski")
                {
                   // await userService.ClearAllUserAsync().ConfigureAwait(false);
                    return View();
                }
            }


            try
            {
               for( int i=0; i< collection.UserToClearListe.Count;i++)
                {
                    if (collection.UserToClearListe[i].Selected)
                    {
                      await  userService.ClearUserAsync(collection.UserToClearListe[i].Name).ConfigureAwait(false);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}