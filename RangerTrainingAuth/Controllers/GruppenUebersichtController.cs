﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Models.GruppenUebersicht;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GruppenUebersichtController : Controller
    {
        private readonly GruppenUebersichtService _gruppenUebersichtService;

        public GruppenUebersichtController(GruppenUebersichtService gruppenUebersichtService)
        {
            _gruppenUebersichtService = gruppenUebersichtService ?? throw new ArgumentNullException(nameof(gruppenUebersichtService));
        }


        public async Task<IActionResult> Index()
        {
            GruppenuebersichtModel viewmodel = new GruppenuebersichtModel();

            viewmodel = await _gruppenUebersichtService.GetAsync();

            return View(viewmodel);
        }
    }
}