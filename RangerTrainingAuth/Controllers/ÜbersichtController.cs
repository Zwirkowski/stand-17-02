﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Models.Übersicht;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
    public class ÜbersichtController : Controller
    {
        public  readonly ÜbersichtsService _übersichtsService;
        private readonly TrainingsTeilnahmeService _trainingsTeilnahmeService;

        public ÜbersichtController(ÜbersichtsService übersichtsService, TrainingsTeilnahmeService trainingsTeilnahmeService)
        {
            if (übersichtsService == null)
                throw new ArgumentNullException(nameof(übersichtsService));

            _übersichtsService = übersichtsService;
            _trainingsTeilnahmeService = trainingsTeilnahmeService ?? throw new ArgumentNullException(nameof(trainingsTeilnahmeService));
        }



        // GET: Übersicht
        public async Task<ActionResult> Index()
        {
            ÜbersichtViewModel übersichtViewModel = new ÜbersichtViewModel();
            übersichtViewModel.Spieler=_übersichtsService.GetSpieler();
            übersichtViewModel.Trainingstage = _übersichtsService.GetSpielerAnTrainingstag(übersichtViewModel.Trainingstage);

            var rows = new List<TableRow>();

            foreach (var tag in übersichtViewModel.Trainingstage.Select(t => t.Datum))
            {
                var row = new TableRow { Date = tag };

                foreach (var spieler in übersichtViewModel.Spieler)
                {
                    string spielername = spieler.Name;

                    var gruppe = await _trainingsTeilnahmeService.GetGruppeImTrainingAsync(tag, spieler.ID);

                    var cell = new TableCell { Gruppe = gruppe };

                    row[spielername] = cell;
                }

                rows.Add(row);
            }

            übersichtViewModel.TableRows = rows;

            return View(übersichtViewModel);
        }

        // GET: Übersicht/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Übersicht/Create
        public ActionResult Create()
        {
            return View();
        }

        //// POST: Übersicht/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(IFormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here


        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Übersicht/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Übersicht/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Übersicht/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Übersicht/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}