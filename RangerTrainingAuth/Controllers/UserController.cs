﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Models.User;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
    public class UserController : Controller
    {
        public UserController(UserService userService)
        {
            if (userService == null)
                throw new ArgumentNullException(nameof(userService));
            UserService = userService;
        }

        public UserService UserService { get; }



        // GET: User
        public ActionResult Index(string namenfilter = null)
        {
            UserViewModel vi = new UserViewModel();
            vi.UserListe = UserService.GetUserListe(namenfilter);

            return View(vi);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        //[HttpGet("Edit/{id}")]
        [HttpGet("[controller]/{id}/Edit")]
        public ActionResult EditUser(string id)
        {
            EditUserViewModel vi = new EditUserViewModel();
            vi = UserService.GetUserDaten(id);

            return View(vi);
        }




        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UebernahmeUser(EditUserViewModel userdaten)
        {
            UserService.SetUserDaten(userdaten);
            return View();
        }


        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}