﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Models.Ergebnis;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
    public class ErgebnisController : Controller
    {

        public ErgebnisController(ErgebnisService ergebnisService)
        {
            ErgebnisService = ergebnisService ?? throw new ArgumentNullException(nameof(ergebnisService));
        }

        public ErgebnisService ErgebnisService { get; }

        public IActionResult Index()
        {

            IndexErgebnisViewModel viewmodel = new IndexErgebnisViewModel();
            viewmodel =   ErgebnisService.GetErgebnisse();
            return View(viewmodel);
        }
    }
}