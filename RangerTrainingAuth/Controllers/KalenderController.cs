﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RangerTrainingAuth.Models.Kalender;
using RangerTrainingAuth.Services;

namespace RangerTrainingAuth.Controllers
{
    public class KalenderController : Controller
    {
        private readonly KalenderEintragsService kalenderEintragsService;

        public KalenderController(KalenderEintragsService kalenderEintragsService)
        {
            this.kalenderEintragsService = kalenderEintragsService ?? throw new ArgumentNullException(nameof(kalenderEintragsService));
        }

        public IActionResult Index()
        {
            return View();
        }

        public Event[] Events()
        {

            return kalenderEintragsService.GetEvents();

            //return new Event[]
            //{
            //    new Event
            //    {
            //        ID = Guid.NewGuid().ToString(),
            //        Title = "Testeintrag",
            //        Start = DateTime.Now,
            //        End = DateTime.Now + TimeSpan.FromHours(4),
            //        BackgroundColor = "#f00000",
            //        TextColor="#000000"
            //    },
            //    new Event
            //    {
            //        ID = Guid.NewGuid().ToString(),
            //        Title = "Training im Forest",
            //        Start = DateTime.Now + TimeSpan.FromDays(1.5),
            //        End = DateTime.Now + TimeSpan.FromDays(3.7),
            //        BackgroundColor = "#0f00f0",
            //        TextColor="#ffff00"

            //    }
            //};
        }
    }
}