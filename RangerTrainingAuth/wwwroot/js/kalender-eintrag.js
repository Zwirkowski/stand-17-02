﻿document.addEventListener('DOMContentLoaded', function () {
    const alleGruppenCheckbox = document.getElementById('alleGruppenCheckbox');

    alleGruppenCheckbox.addEventListener('change', function () {
        applyChecked();
    });

    applyChecked();

    StartDatum.addEventListener('change', function () {

        if (EndDatum.value < StartDatum.value) {

            EndDatum.value = StartDatum.value;
        }
    });

    Anlass.addEventListener('change', function () {
        if (Anlass.value === 'Saisonstart' || Anlass.value === 'Saisonende') {

            EndDatum.value = StartDatum.value;
        }
    });


});

function applyChecked() {
    const isChecked = alleGruppenCheckbox.checked;
    const groupCheckboxes = document.getElementsByClassName('gruppen-checkbox');

    for (let checkbox of groupCheckboxes) {
        checkbox.disabled = isChecked;

        if (isChecked === true) {
            checkbox.checked = isChecked;
        }
    }
}
