﻿document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['dayGrid'],
        locale: 'de',
        events: '/Kalender/Events',
  
        eventClick: function (args)
        {
  

            $('#popover-title').text(args.event.title);

            $('#event-id').val(args.event.id);

            const element = $(args.el);

            element.popover({
                placement: 'bottom',
                title: 'Eventinfos',
                html: true,
              content: $('#popover-delete-form').clone()
 
            });

            element.popover('toggle');

            console.info('Ich bin bis hier hin gekommen.');
        }
    });

    $('.fc-event').popover({
        placement: 'bottom',
        title: 'Eventinfos',
        html: true
    });

    calendar.render();
});