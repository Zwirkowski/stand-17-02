﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Entities
{
    [DebuggerDisplay("{ID} (Gruppe {Gruppe})")]
    public class SpielerImTraining 
    {
        public int ID { get; set; }
        public int Gruppe { get; set; }
        public bool Anwesend { get; set; }
        public bool Eneabled { get; set; }
        public bool IstGruppenleiter { get; set; }
        public Spieler Spieler { get; set; }
        public DateTime TrainingsTag { get; set; }

    }
}
