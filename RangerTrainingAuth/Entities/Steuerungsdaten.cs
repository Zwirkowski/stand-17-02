﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Entities
{
    public class Steuerungsdaten
    {
        public int ID { get; set; }
        public int TradingFürGruppeErlaubt { get; set; }

    }
}
