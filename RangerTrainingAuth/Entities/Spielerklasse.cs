﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Entities
{
    public enum Spielerklasse
    {
        Fan = 0,
        DefenceLine =1,
        DefenceBackField =2,
        OffensLine = 3,
        OffensBackField = 4,
        Trainer,
        HeadCoach
    }
}
