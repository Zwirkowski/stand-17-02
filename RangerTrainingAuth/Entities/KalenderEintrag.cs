﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Entities
{
    [DebuggerDisplay("{Anlass} ({Date} - {EndDate})")]
    public class KalenderEintrag
    {
        public int ID { get; set; }
        public int Gruppe { get; set; }
        public bool AlleGruppen { get; set; }
        public String Bezeichnung { get; set; }  
        public DateTime Date { get; set; } 
        public DateTime EndDate { get; set; }  
        public string Anlass { get; set; }  
        public bool SerienTermin { get; set; }
        public Guid SerienNummer { get; set; }

        internal KalenderEintrag Copy()
        {
            KalenderEintrag ke = new KalenderEintrag();

            ke.ID = ID;
            ke.Gruppe = Gruppe;
            ke.Bezeichnung = Bezeichnung;
            ke.Date = Date;
            ke.EndDate = EndDate;
            ke.Anlass = Anlass;
            ke.SerienTermin = SerienTermin;
            ke.SerienNummer = SerienNummer;

            return ke;
        }
    }
}
