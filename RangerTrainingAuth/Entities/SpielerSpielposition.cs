﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Entities
{
    public class SpielerSpielposition
    {
        public Spieler Spieler { get; set; }
        public int SpielerID { get; set; }
        public Spielposition Spielposition { get; set; }
        public int SpielpositionID { get; set; }
    }
}
