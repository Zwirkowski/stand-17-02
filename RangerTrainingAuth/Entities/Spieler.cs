﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Entities
{
    [DebuggerDisplay("{Vorname} {Nachname} ({Spielposition})")]
    public class Spieler
    {
        public int ID { get; set; }
        public int SpielerNummer { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        [Range(0, EntityKonstanten.MaximaleGruppennummer)]
        public int Trainingsgruppe { get; set; }
        public DateTime Geburtstag { get; set; }
        public Spielerklasse Klasse { get; set; }
        public List<SpielerSpielposition> Spielpositionen { get; set; } = new List<SpielerSpielposition>();

        public string UserId { get; set; }
        public bool Deaktiviert { get; set; }
        public ApplicationUser User { get; set; }
        public string Name => $"{Vorname} {Nachname}";
    }

}
