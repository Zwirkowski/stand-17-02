﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public Spieler Spieler { get; set; }
        [Range(0,EntityKonstanten.MaximaleGruppennummer)]
        public int Trainergruppe { get; set; }


    }
}
