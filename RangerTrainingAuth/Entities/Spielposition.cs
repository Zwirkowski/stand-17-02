﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Entities
{
    [DebuggerDisplay("{NameAbkürzung}")]
    public class Spielposition
    {
        public int ID { get; set; }
        public string Name { get; set; } = "unbekannt";
        public string NameAbkürzung { get; set; } = "?";
        public string Gruppe { get; set; } = "Gruppe unbekannt";
        public virtual ICollection<SpielerSpielposition> Spieler { get; set; }
    }
}
