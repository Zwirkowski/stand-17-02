﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RangerTrainingAuth.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RangerTrainingAuth.Entities;
using RangerTrainingAuth.Services;
using Microsoft.Extensions.Logging;
using RangerTrainingAuth.Tests;
//using RangerTrainingAuth.Sonstiges;

namespace RangerTrainingAuth
{
    public class Startup
    {
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;

         //  DateTime heute = new DateTime();
            int kw = new DateTime(2020, 1, 7).GetKW();
            Logger = logger;
            //  int KW = Kalenderwoche.KW(heute);

        }

        public IConfiguration Configuration { get; }
        private ILogger Logger { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            var connectionStringBuilder = new Npgsql.NpgsqlConnectionStringBuilder();
            connectionStringBuilder.Host = Configuration.GetValue<string>("ConnectionStrings:DefaultConnection:Server");
            connectionStringBuilder.Port = Configuration.GetValue<int>("ConnectionStrings:DefaultConnection:Port");
            connectionStringBuilder.Username = Configuration.GetValue<string>("ConnectionStrings:DefaultConnection:User");
            connectionStringBuilder.Password = Configuration.GetValue<string>("ConnectionStrings:DefaultConnection:Password");

            Console.WriteLine("=======================" + connectionStringBuilder.ConnectionString);

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(connectionStringBuilder.ConnectionString)
            );
            services.AddDefaultIdentity<ApplicationUser>(options => 
            {
                options.User.AllowedUserNameCharacters += " öäüÖÄÜß";
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            })
                    .AddRoles<IdentityRole>()
                    .AddDefaultUI(UIFramework.Bootstrap4)
                    .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddTransient<DbUpdater>();
            services.AddTransient<SpielerService>();
            services.AddTransient<SpielPositionService>();
            services.AddTransient<GruppenUebersichtService>();
            services.AddTransient<GruppenleiterService>();
            services.AddTransient<TesteAdminVorhanden>();
            services.AddTransient<KalenderEintragsService>();
            services.AddTransient<TrainingsTeilnahmeService>();
            services.AddTransient<SteuerungsService>();
            services.AddTransient<ErgebnisService>();
            services.AddTransient<UserService>();
            services.AddTransient<ÜbersichtsService>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DbUpdater dbUpdater, TesteAdminVorhanden testeAdminVorhanden)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
           //     app.UseHsts();
            }

         //   app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            testeAdminVorhanden.Run().GetAwaiter().GetResult();
            dbUpdater.Run().GetAwaiter().GetResult();

            Test test = new Test();

                test.Kalenderwochentest();
        }
    }
}
