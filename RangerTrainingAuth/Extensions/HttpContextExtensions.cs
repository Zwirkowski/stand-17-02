﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using RangerTrainingAuth.Entities;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Mvc
{
    public static class HttpContextExtensions
    {
        public static async Task RefreshAuthCookieAsync(this HttpContext context)
        {
            if (context is null) return;

            if (context.User.Identity.IsAuthenticated)
            {
                var services = context.RequestServices;

                var signInManager = services.GetRequiredService<SignInManager<ApplicationUser>>();
                var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
                //var roleManager = services.GetRequiredService<RoleManager<ApplicationUser>>();

                var user = await userManager.FindByNameAsync(context.User.Identity.Name);

                var roles = await userManager.GetRolesAsync(user);

                await signInManager.RefreshSignInAsync(user);
            }
        }
    }
}
