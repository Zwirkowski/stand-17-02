﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Views.User
{
    public class UserSpielposition
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Auswahl { get; set; }

    }
}
