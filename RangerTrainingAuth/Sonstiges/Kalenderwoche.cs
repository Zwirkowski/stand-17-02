﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace System
{
    public static class Kalenderwoche
    {
        public static int GetKW(this DateTime Datum)
            // gibt die Kalenderwoche des Datums zurück
            // KW beginnt am Montag
        {
            CultureInfo CUI = CultureInfo.CurrentCulture;
            //  int _kw= CUI.Calendar.GetWeekOfYear(Datum, CUI.DateTimeFormat.CalendarWeekRule, CUI.DateTimeFormat.FirstDayOfWeek);
              int _kw= CUI.Calendar.GetWeekOfYear(Datum, CUI.DateTimeFormat.CalendarWeekRule, System.DayOfWeek.Monday);
            int Jahr = Datum.Year;

            // Rückgabeformat 202003 == 2020(Jahr)03(KW)
            return (Jahr*100) + ((_kw > 52) ? 1 : _kw);
        }
    }
}
