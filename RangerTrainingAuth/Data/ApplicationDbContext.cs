﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RangerTrainingAuth.Entities;

namespace RangerTrainingAuth.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }


        public DbSet<Spielposition> Spielposition{ get; set; }
        public DbSet<Spieler> Spieler{ get; set; }
        public DbSet<KalenderEintrag> KalenderEintrags{ get; set; }
        public DbSet<Steuerungsdaten> SteuerungsDaten { get; set; }
        public DbSet<SpielerImTraining> SpielerImTraining { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<SpielerSpielposition>().HasKey(nameof(SpielerSpielposition.SpielerID), nameof(SpielerSpielposition.SpielpositionID));
            builder.Entity<SpielerSpielposition>().HasOne(s => s.Spieler).WithMany(s => s.Spielpositionen).HasForeignKey(k=>k.SpielerID);
            builder.Entity<SpielerSpielposition>().HasOne(s => s.Spielposition).WithMany(s => s.Spieler).HasForeignKey(k=>k.SpielpositionID);
        }
    }

}
