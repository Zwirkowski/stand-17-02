﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using RangerTrainingAuth.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Data
{
    public class TesteAdminVorhanden
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext context;

        public TesteAdminVorhanden(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext context)
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _userManager = userManager;
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public  async Task  Run()
        {
            await context.Database.EnsureCreatedAsync();

            #region rollen definieren
            var role = await _roleManager.FindByNameAsync(Rolename.Admin);
            if (role == null)
            {
                await _roleManager.CreateAsync(new IdentityRole(Rolename.Admin));
            }

            {
                var spielerRolle = await _roleManager.FindByNameAsync(Rolename.Spieler);

                if (spielerRolle == null)
                {
                    await _roleManager.CreateAsync(new IdentityRole(Rolename.Spieler));
                }
            }

            {
                var gruppenleiterRolle = await _roleManager.FindByNameAsync(Rolename.Gruppenleiter);

                if (gruppenleiterRolle == null)
                {
                    await _roleManager.CreateAsync(new IdentityRole(Rolename.Gruppenleiter));
                }
            }

            #endregion


            var user = _userManager.Users.FirstOrDefault(u => u.UserName == "Georg Zwirkowski");
            if (user == null)
            {
                ApplicationUser newuser = new ApplicationUser()
                {
                    UserName = "Georg Zwirkowski",
                    Email = "georg@zwirkowski.de",
                    EmailConfirmed = true
                };

                await _userManager.CreateAsync(newuser, "password");
                await _userManager.AddToRoleAsync(newuser, Rolename.Admin);
            }
            else
            {
                if (!await _userManager.IsInRoleAsync(user, Rolename.Admin))
                {
                    await _userManager.AddToRoleAsync(user, Rolename.Admin);
                }
            }
        }
    }


}
