﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using RangerTrainingAuth.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace RangerTrainingAuth.Data
{
    public class DbUpdater
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> _userManager;

        public DbUpdater(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            this.context = context;
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        
        public async Task Run()
            {
            var root =Directory.GetParent(Assembly.GetExecutingAssembly().Location);
            string filepath= Path.Combine(root.FullName, "Data","Defaults", "Position.csv");

            if (File.Exists(filepath))
            {
                List<String[]> inputliste = new List<String[]>();

                System.IO.StreamReader sr = new System.IO.StreamReader(filepath);
                while (sr.Peek() != -1) inputliste.Add(sr.ReadLine().Split(';'));
                sr.Close();


                foreach (string[] Inputstring in inputliste.Skip(1))
                {
                    Spielposition position = context.Spielposition.FirstOrDefault(sp =>
                                                                    sp.Name == Inputstring[2] &&
                                                                    sp.NameAbkürzung == Inputstring[3]);

                    if(position == null)
                    {
                        position = new Spielposition()
                        {
                            Name = Inputstring[2],
                            NameAbkürzung = Inputstring[3],
                            Gruppe = Inputstring[1]

                        };
                       context.Spielposition.Add(position);
                    }
                 }

                context.SaveChanges();

            }

            
            filepath= Path.Combine(root.FullName, "Data","Defaults", "Spieler.csv");
            var useranzahl =   _userManager.Users.Count();

            if (File.Exists(filepath) &&(useranzahl <= 1))
            {
                List<String[]> inputliste = new List<String[]>();

                System.IO.StreamReader sr = new System.IO.StreamReader(filepath);
                while (sr.Peek() != -1) inputliste.Add(sr.ReadLine().Split(';'));
                sr.Close();


                foreach (string[] Inputstring in inputliste.Where(i => i != null))
                {
                    Debug.WriteLine(Inputstring[2]);
                    string nummer = Inputstring[0];
                    Spieler spieler = context.Spieler.Include(s => s.Spielpositionen)
                                                     .FirstOrDefault(s =>
                                                                   s.Vorname == Inputstring[2] &&
                                                                   s.Nachname == Inputstring[3]);
                    if (string.IsNullOrEmpty(Inputstring[0]))
                    {
                        Inputstring[0] = "100";

                    }
                    if (spieler == null)
                    {


                        spieler = new Spieler()
                        {
                            SpielerNummer= int.Parse(Inputstring[0]),
                            Vorname = Inputstring[2],
                            Nachname = Inputstring[3],
                            Trainingsgruppe = 0,


                        };

                        var sp = context.Spielposition.FirstOrDefault(p => p.NameAbkürzung == Inputstring[1]);

                        if (sp != null && !spieler.Spielpositionen.Any(p => p.SpielpositionID == sp.ID)) spieler.Spielpositionen.Add(new SpielerSpielposition() {
                            Spieler=spieler,
                            Spielposition=sp
                        });


                        context.Spieler.Add(spieler);
   
                    }
                    else
                    {


                       if(int.TryParse(Inputstring[0],out int spn)) spieler.SpielerNummer = spn;
                        var sp = context.Spielposition.FirstOrDefault(p => p.NameAbkürzung == Inputstring[1]);

                        if (sp != null && !spieler.Spielpositionen.Any(p => p.SpielpositionID == sp.ID)) spieler.Spielpositionen.Add(new SpielerSpielposition()
                        {
                            Spieler = spieler,
                            Spielposition = sp
                        });
                    }


                    string username = $"{spieler.Vorname} {spieler.Nachname}";
                    var user = _userManager.Users.FirstOrDefault(u => u.UserName == username);

                    if (user == null)
                    {
                        ApplicationUser newuser = new ApplicationUser()
                        {
                            UserName = username,
                            Email = spieler.Vorname + "." + spieler.Nachname + "@zwirkowski.de",
                            Spieler = spieler
                        };

                        await _userManager.CreateAsync(newuser, "password").ConfigureAwait(false);
                        await _userManager.AddToRoleAsync(newuser, Rolename.Spieler).ConfigureAwait(false);

                    }
                    else
                    {
                        user.Spieler = spieler;
                    }

                }

                context.SaveChanges();
            }


        }




    }
}
