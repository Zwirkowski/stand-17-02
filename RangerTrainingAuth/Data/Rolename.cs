﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Data
{
    public static class Rolename
    {
        public const string Admin = "Admin";
        public const string Spieler = "Spieler";
        public const string Gruppenleiter = "Gruppenleiter";
    }
}
