﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RangerTrainingAuth.Data
{
    public static class KalenderEventNamen
    {
        public const string Training = "Training";
        public const string FlexTraining = "Flextraining";
        public const string SaisonStart = "Saisonstart";
        public const string SaisonEnde = "Saisonende";
        public const string Heimspiel = "Heimspiel";
        public const string Auswertsspiel = "Ausswertsspiel";
        public const string Sonstiges = "Sonstiges";
        public const string Teamsitzung = "Teamsitzung";
        public const string Geburtstag = "Geburtstag";
    }
}
